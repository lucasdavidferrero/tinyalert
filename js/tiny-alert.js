(function () {
    'use strict';
    const triggerButtons = document.querySelectorAll('.trigger-btn');
    const alerts = document.querySelectorAll('.tinyalert');
    const dimmer = document.querySelector('.tinyalert-dimmer');

    let currentAlertOnScreen;
    let currentCloseActions;
    function handleTrigger() {
        const dataTarget = this.dataset.target;
        if (dataTarget) {
            let alertHooked;
            alerts.forEach(function (currentAlert) {
                if (currentAlert.getAttribute('id') === dataTarget.slice(1, dataTarget.length)) {
                    alertHooked = currentAlert;
                }
            });
            if (alertHooked) {
                // Show the dimmer and alert.
                currentAlertOnScreen = alertHooked;
                showAlertAndDimmer(alertHooked);
            } else {
                // Show some console error.
                console.error(`We haven't match any alert with the target ${dataTarget}. Make sure you have an alert with that ID.`);
            }
        } else {
            console.error(`The element ${this.tagName} with the content "${this.textContent}" doesn't have the required data-target attribute. Please include its attribute with the respective value. For further info, check out the tinyalert's page.`);
        }
    }

    function showAlertAndDimmer(alertElem) {
        document.body.style.overflowY = 'hidden'; // This prevents scroll on the body when the modal is showing.
        dimmer.classList.add('show-dimmer');
        alertElem.classList.add('show-alert');
        setTimeout(() => {
            alertElem.classList.add('animate-alert');
        }, 50);
        dimmer.setAttribute('aria-hidden', 'false');
        alertElem.setAttribute('aria-hidden', 'false');
        //listen to closebtns
        currentCloseActions = alertElem.querySelectorAll('[data-action="close"]');
        listenToCloseActions(currentCloseActions);
    }

    function closeAlertAndDimmer() {
        clearEventListenersFromCloseActions(currentCloseActions);
        if (currentAlertOnScreen) {
            dimmer.setAttribute('aria-hidden', 'true');
            currentAlertOnScreen.setAttribute('aria-hidden', 'true');
            currentAlertOnScreen.classList.remove('animate-alert');
            currentAlertOnScreen.addEventListener('transitionend', animateAlert);
            currentAlertOnScreen = '';
        }
        dimmer.classList.remove('show-dimmer');
        document.body.style.overflowY = 'initial';
    }

    function animateAlert() {
        if (!this.classList.contains('animate-alert')) {
            this.classList.remove('show-alert');
        }
        this.removeEventListener('transitionend', () => { });
    }

    function listenToCloseActions(nodeList) {
        nodeList.forEach(function (el, i) {
            el.addEventListener('click', closeAlertAndDimmer);
        });
    }
    function clearEventListenersFromCloseActions(nodeList) {
        function dummy() {
            // We do this because to remove an event listener
            // we can't declare the callback inside the argument. It's just a rule.
        }
        nodeList.forEach(function (el) {
            el.removeEventListener('click', dummy);
        });
    }
    triggerButtons.forEach(function (currentEl) {
        currentEl.addEventListener('click', handleTrigger);
    });
    dimmer.addEventListener('click', closeAlertAndDimmer)
})();

/*TODO: Set the tabindex properly 
for accessibility purpose. 
*/